
#ifndef SPIDEV_H_
#define SPIDEV_H_

#include <linux/spi/spidev.h>

namespace videray {

class SpiDev {
public:
    SpiDev();
    ~SpiDev();
    int open(const char* filepath);
    int close();

    int config(uint32_t hz, uint8_t bpw, uint32_t mode);

    int transfer(struct spi_ioc_transfer*, int num);

private:
    int mFD;
};

}


#endif // SPIDEV_H_