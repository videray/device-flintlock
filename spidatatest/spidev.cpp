

/**
 * https://manpages.debian.org/wheezy-backports/linux-manual-3.16/struct_spi_transfer.9.en.html
 * 
 */ 


#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <unistd.h>
#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif

#include <cerrno>

#include "spidev.h"

namespace videray {

SpiDev::SpiDev()
 : mFD(-1)
{
}

SpiDev::~SpiDev()
{
    if(mFD >= 0) {
        //should log that we did not close before exit
        close();
    }
}

int SpiDev::open(const char* filepath)
{
    mFD = ::open(filepath, O_RDWR);
    return mFD < 0;
}

int SpiDev::close()
{
    ::close(mFD);
    mFD = -1;
    return 0;
}

int SpiDev::config(uint32_t hz, uint8_t bpw, uint32_t mode)
{
    int ret;
    ret = ::ioctl(mFD, SPI_IOC_WR_MAX_SPEED_HZ, &hz);
    if(ret < 0) {
        return ret;
    }

    ret = ::ioctl(mFD, SPI_IOC_WR_MODE32, &mode);
    if(ret < 0) {
        return ret;
    }

    ret = ::ioctl(mFD, SPI_IOC_WR_BITS_PER_WORD, &bpw);
    if(ret < 0) {
        return ret;
    }

    return ret;
}

int SpiDev::transfer(struct spi_ioc_transfer* transfers, int num)
{
    int ret;
    errno = 0;
    ret = ::ioctl(mFD, SPI_IOC_MESSAGE(num), transfers);
    return errno;
}

} // namespace