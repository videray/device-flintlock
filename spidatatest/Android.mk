LOCAL_PATH := $(call my-dir)

 

include $(CLEAR_VARS) 

LOCAL_MODULE := spidatatest
LOCAL_MODULE_TAGS := optional 
LOCAL_SRC_FILES := spidev.cpp spidatatest.cpp
LOCAL_SHARED_LIBRARIES := liblog 
LOCAL_CFLAGS := 
LOCAL_C_INCLUDES := bootable/recovery
LOCAL_C_INCLUDES += external/vboot_reference/firmware/include 

include $(BUILD_EXECUTABLE)