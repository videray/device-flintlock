
#include <cstring>
#include <cstdlib>
#include <cstdint>

#include "spidev.h"

using namespace videray;

#define SPEED 1000000
#define READAMOUNT 16

int main()
{
	int ret;
	SpiDev spi;

	ret = spi.open("/dev/spidev0.0");
	if(ret != 0) {
		printf("error opening %d\n", ret);
	}

	ret = spi.config(SPEED, 16, 0);
	if(ret != 0) {
		printf("error configing %d\n", ret);
	}

	struct spi_ioc_transfer transfer[2];
	memset(transfer, 0, sizeof(transfer));

	uint8_t tx_buf[4];
	tx_buf[0] = 0;
	tx_buf[1] = 1;
	tx_buf[2] = 0;
	tx_buf[3] = 1;

	transfer[0].tx_buf = (__u64)tx_buf;
	transfer[0].len = 4;
	transfer[0].speed_hz = SPEED;
	transfer[0].bits_per_word = 16;

	uint8_t rx_buf[READAMOUNT];
	transfer[1].rx_buf = (__u64)rx_buf;
	transfer[1].len = READAMOUNT;
	transfer[1].speed_hz = SPEED;
	transfer[1].bits_per_word = 16;

	/*
	uint8_t rx_buf2[READAMOUNT];
	transfer[2].rx_buf = (__u64)rx_buf2;
	transfer[2].len = READAMOUNT;
	transfer[2].speed_hz = SPEED;
	transfer[2].bits_per_word = 16;
	*/

	ret = spi.transfer(transfer, 2);
	if(ret != 0) {
		printf("error transfer: %d\n", ret);
		perror("wtf");
	}

	printf("success transfer %d bytes\n", READAMOUNT);

	

	return ret;
}
