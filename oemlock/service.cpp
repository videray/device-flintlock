#define LOG_TAG "android.hardware.oemlock@1.0-service"
#include <utils/Log.h>

#include <hidl/HidlTransportSupport.h>

#include "OemLock.h"


using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;
using ::android::hardware::oemlock::V1_0::implementation::OemLock;
using ::android::OK;
using ::android::sp;



int main( int /* argc */, char* /*argv */ [] )
{

  ALOGI( "Service starting %s", "v1.0" );

  sp<OemLock> device = new OemLock;

  configureRpcThreadpool( 1 /*threads*/, true /*willJoin*/ );


  if( device->registerAsService() != OK ) {
    ALOGE( "Could not register service." );
    return 1;
  }

  joinRpcThreadpool();

  // joinRpcThreadpool should never return
  ALOGE( "Service exited!" );
  return 1;
}
