#include "OemLock.h"

namespace android {
namespace hardware {
namespace oemlock {
namespace V1_0 {
namespace implementation {

// Methods from ::android::hardware::oemlock::V1_0::IOemLock follow.
Return<void> OemLock::getName(getName_cb _hidl_cb) {
    _hidl_cb(OemLockStatus::OK, "Videray");
    return Void();
}

Return<::android::hardware::oemlock::V1_0::OemLockSecureStatus> OemLock::setOemUnlockAllowedByCarrier(bool allowed, const hidl_vec<uint8_t>& signature) {
    // TODO implement
    return ::android::hardware::oemlock::V1_0::OemLockSecureStatus {};
}

Return<void> OemLock::isOemUnlockAllowedByCarrier(isOemUnlockAllowedByCarrier_cb _hidl_cb) {
    _hidl_cb(OemLockStatus::OK, true);
    return Void();
}

Return<::android::hardware::oemlock::V1_0::OemLockStatus> OemLock::setOemUnlockAllowedByDevice(bool allowed) {
    // TODO implement
    return ::android::hardware::oemlock::V1_0::OemLockStatus {};
}

Return<void> OemLock::isOemUnlockAllowedByDevice(isOemUnlockAllowedByDevice_cb _hidl_cb) {
    _hidl_cb(OemLockStatus::OK, true);
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

//IOemLock* HIDL_FETCH_IOemLock(const char* /* name */) {
    //return new OemLock();
//}
//
}  // namespace implementation
}  // namespace V1_0
}  // namespace oemlock
}  // namespace hardware
}  // namespace android
