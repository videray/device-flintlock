
UBOOT_SRC_DIR := vendor/nxp-opensource/uboot-imx
UBOOT_CFG_NAME := imx8mq_frizzen_android_defconfig

KERNEL_SRC_DIR := vendor/nxp-opensource/kernel_imx
KERNEL_CFG_NAME := videray_frizzen_android_defconfig
BOARD_DTB_FILE_NAME := videray-frizzen-dcss-koe.dtb

include device/videray/imx8m_dart/imx8m-dart_BoardConfig.mk


SF_PRIMARY_DISPLAY_ORIENTATION := 270


BOARD_SEPOLICY_DIRS += \
       device/videray/flintlock/sepolicy


PRODUCT_SYSTEM_VERITY_PARTITION := /dev/block/platform/30b40000.usdhc/by-name/system
PRODUCT_VENDOR_VERITY_PARTITION := /dev/block/platform/30b40000.usdhc/by-name/vendor


TARGET_RECOVERY_FSTAB := device/videray/flintlock/recovery.fstab
TARGET_RELEASETOOLS_EXTENSIONS := device/videray/flintlock

#BOARD_KERNEL_CMDLINE := root=/dev/ram0
#BOARD_KERNEL_CMDLINE := console=ttymxc0,115200 earlycon=imxuart,0x30860000,115200 androidboot.console=ttymxc0 
# BOARD_KERNEL_CMDLINE += androidboot.hardware=flintlock
BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive

#### Partition Scheme
#
# see uboot/include/configs/imx8m_flintlock.h
# boot 		32M
# recovery    	48M
# misc		       128k
# system	       1.7G
# cache		512M
# vendor	       200M
# userdata	       6G
# sdcard             ~LEFTOVER

# Support gpt
BOARD_BPT_INPUT_FILES += device/videray/flintlock/flintlock-partitions.bpt

TARGET_USERIMAGES_USE_EXT4 := true

BOARD_BOOTIMAGE_PARTITION_SIZE := 33554432

BOARD_RECOVERYIMAGE_PARTITION_SIZE := 50331648

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1782579200
#BOARD_SYSTEMIMAGE_FILE_SYSTEM_TYPE := squashfs
BOARD_SYSTEMIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_USERDATAIMAGE_PARTITION_SIZE := 6442450944
BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_CACHEIMAGE_PARTITION_SIZE := 734003200
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4

BOARD_DTBOIMG_PARTITION_SIZE := 4194304

BOARD_VENDORIMAGE_PARTITION_SIZE := 209715200
#BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := squashfs
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
TARGET_COPY_OUT_VENDOR := vendor

BOARD_FLASH_BLOCK_SIZE := 512

DEVICE_MANIFEST_FILE := device/videray/flintlock/vintf/manifest.xml
DEVICE_MANIFEST_FILE += device/videray/flintlock/vintf/vendor.hw.backscatter.xml


TARGET_USES_64_BIT_BINDER := true

TARGET_RECOVERY_UI_LIB := librecovery_ui_flintlock
