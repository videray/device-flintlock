LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := v4l2_videocapture
LOCAL_VENDOR_MODULE := true
LOCAL_SRC_FILES := v4l2_capture.cpp
LOCAL_SHARED_LIBRARIES := liblog
LOCAL_CPPFLAGS += -g -O0
LOCAL_C_INCLUDES +=

include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)

LOCAL_MODULE := opencl_test
LOCAL_VENDOR_MODULE := true
LOCAL_SRC_FILES := opencl_test.cpp
LOCAL_SHARED_LIBRARIES := liblog libcutils libutils libOpenCL
LOCAL_CPPFLAGS += -g -O0
LOCAL_C_INCLUDES += \
	$(FSL_PROPRIETARY_PATH)/fsl-proprietary/include

include $(BUILD_EXECUTABLE)