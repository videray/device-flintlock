#!/bin/bash

astyle -n --options=astyle.cfg --recursive "*.cpp"
astyle -n --options=astyle.cfg --recursive "*.h"
