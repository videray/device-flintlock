

# Disable Android Verified Boot for now
BOARD_BUILD_DISABLED_VBMETAIMAGE := true


$(call inherit-product, sdk/build/product_sdk.mk)
$(call inherit-product, development/build/product_sdk.mk)

#$(call inherit-product, device/videray/minimal-embedded-system.mk)
#$(call inherit-product, $(SRC_TARGET_DIR)/product/embedded.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/generic.mk)
$(call inherit-product, device/videray/imx8m_dart/imx8m-dart_device.mk)
#$(call inherit-product, vendor/videray/device/backscatter-device.mk)


PRODUCT_NAME := flintlock
PRODUCT_DEVICE := flintlock
PRODUCT_BRAND := Videray
PRODUCT_MODEL := PX1
PRODUCT_MANUFACTURER := Videray
PRODUCT_CHARACTERISTICS := device
PRODUCT_OTA_PUBLIC_KEYS := /home/jenkins/videray-certs/releasekey.x509.pem

PRODUCT_PACKAGES += \
	busybox \
	videray-storage \
	vendor.videray.hardware.backscatter@1.0-service \
	SparkLauncher \
	VideraySystemUpdater \
	BackscatterApp \
	Chromium \
	vdrprocess \
	flintlocktool \
	stm32flash \
	flashrom \
	libbackscatter_jni \
	libIllusion \
	BackscatterApp \
	VideraySystemUpdater \
	libvdr \
	libvideray_core


## OEM lock
PRODUCT_PACKAGES += \
	android.hardware.oemlock@1.0-service \


## Sierra wireless
PRODUCT_PACKAGES += \
	gps.imx8.so \
	init.dhcpcd \
	init.config.ip \
	swihltrace \
	slqssdk \
	SierraDMLog \
	SierraFwDl7xxx \
	SierraImgMgr \
	SierraSARTool \
	DownloadTool \
	libsierra-ril.so \
	libsierraat-ril.so \
	libsierrahl-ril.so \
	libswims.so \
	libswiqmiapi.so \
	libDownloadTool.so \
	sierra_logs_app \
	swifota_app \
	swiomadm_app \
	simswitcher_app \
	rild

PRODUCT_COPY_FILES += \
	device/sample/etc/apns-full-conf.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/apns-conf.xml

#GNSS HAL
PRODUCT_PACKAGES += \
	android.hardware.gnss@1.0-service \
    android.hardware.gnss@1.0-impl:64

PRODUCT_COPY_FILES += \
	device/common/gps/gps.conf_US:system/etc/gps.conf

#USE_CAMERA_V4L2_HAL := true
#PRODUCT_PACKAGES += camera.v4l2
#PRODUCT_PROPERTY_OVERRIDES += ro.hardware.camera=v4l2

#PRODUCT_PACKAGES += camera.videray
#PRODUCT_PROPERTY_OVERRIDES += ro.hardware.camera=videray
#android.hardware.camera.provider@2.4-service

#PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.device@1.0-impl \
    camera.device@3.2-impl \
    camera.imx8

#PRODUCT_COPY_FILES += \
    device/videray/flintlock/external_camera_config.xml:$(TARGET_COPY_OUT_VENDOR)/etc/external_camera_config.xml \
    frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \

# Init and recovery general configuration
PRODUCT_COPY_FILES += \
	device/videray/flintlock/init.flintlock.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.flintlock.rc \
	device/videray/flintlock/init.sd.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.sd.rc \
	device/videray/flintlock/init.emmc.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.emmc.rc \
	device/videray/flintlock/init.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.usb.rc \
	device/videray/flintlock/init.wifi.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.wifi.rc \
	device/videray/flintlock/init.wwan.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.wwan.rc \
	device/videray/flintlock/init.lcd.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.lcd.rc \
	device/videray/flintlock/ueventd.flintlock.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/videray/flintlock/fstab.flintlock:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.flintlock \
	device/videray/flintlock/init.recovery.flintlock.rc:root/init.recovery.flintlock.rc \
	device/videray/flintlock/input/Atmel_maXTouch_Touchscreen.idc:/vendor/usr/idc/Atmel_maXTouch_Touchscreen.idc \

PRODUCT_COPY_FILES += \
    device/videray/flintlock/public.libraries.txt:/vendor/etc/public.libraries.txt

# Hardware declaration
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.screen.portrait.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.screen.portrait.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.distinct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.distinct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.xml \
	frameworks/native/data/etc/android.hardware.location.gps.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level-0.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_0_3.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version-1_0_3.xml \
	frameworks/native/data/etc/android.software.app_widgets.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.app_widgets.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	device/videray/flintlock/bootanimation.zip:$(TARGET_COPY_OUT_SYSTEM)/media/bootanimation.zip \

PRODUCT_COPY_FILES += \
	device/videray/flintlock/privapp-permissions-flintlock.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/privapp-permissions-flintlock.xml \

PRODUCT_PROPERTY_OVERRIDES += \
	ro.backlight.dev=backlight_mipi_dsi \
	ro.sf.lcd_density=313 \
	dalvik.vm.heapgrowthlimit=192m \
	dalvik.vm.heapsize=512m \
	ro.build.channel=beta \
	ro.build.version.id=0.4.2 \
	ro.build.updateserver=https://update.videray.com


PRODUCT_AAPT_CONFIG += xlarge large hdpi xhdpi

DEVICE_PACKAGE_OVERLAYS := device/videray/flintlock/overlay
