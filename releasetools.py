
import common

def FlashSTM32(info):
    info.script.AppendExtra(
        """
        ui_print("flashing STM32...");
        package_extract_file("stm32.hex", "stm32.hex");
        if (run_program("/sbin/flintlocktool", "flash_micro", "-f", "stm32.hex")) != 0 then
          abort("error running flintlocktool flash_micro");
        endif;
        ui_print("flashing STM32 done");
        """
    )

def FlashFPGA(info):
    info.script.AppendExtra(
        """
        ui_print("flashing FPGA...");
        package_extract_file("fpga.bin", "fpga.bin");
        run_program("/sbin/flintlocktool", "flash_FPGA", "-f", "fpga.bin");
        ui_print("flashing FPGA done");
        """
    )

def FullOTA_InstallEnd(info):
    fpga_bin = info.input_zip.read("RADIO/fpga.bin")
    common.ZipWriteStr(info.output_zip, "fpga.bin", fpga_bin)
    FlashFPGA(info)

    stm32_dat = info.input_zip.read("RADIO/stm32.hex")
    common.ZipWriteStr(info.output_zip, "stm32.hex", stm32_dat)
    FlashSTM32(info)


def IncrementalOTA_InstallEnd(info):
    source_fpga_bin = info.source_zip.read("RADIO/fpga.bin")
    target_fpga_bin = info.target_zip.read("RADIO/fpga.bin")
    if source_fpga_bin != target_fpga_bin:
        common.ZipWriteStr(info.output_zip, "fpga.bin", target_fpga_bin)
        FlashFPGA(info)

    source_stm32_dat = info.source_zip.read("RADIO/stm32.hex")
    target_stm32_dat = info.target_zip.read("RADIO/stm32.hex")
    if source_stm32_dat != target_stm32_dat:
        common.ZipWriteStr(info.output_zip, "stm32.hex", target_stm32_dat)
        FlashSTM32(info)
