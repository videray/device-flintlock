

Fresh Boot partition
=========================

The IMX8 SoM comes from Variscite with some version of uboot that tries to boot from ethernet device. We will be using this to bootstrap the Videray firmware.

 * Insert special "Bootstrap" Sdcard
 * Stop normal boot by pressing any key
 * run contents of uboot_script.sh
 * switch boot mode to emmc
 * use fastboot to flash rest of the system

#### remember to format /storage ###
the system will not boot unless the /storage partition is formatted

```
mke2fs /dev/block/by-name/storage
```

####### Create Bootstrap Sdcard ############

```
$ cd device/videray/flintlock/util_sdcard
$ make util_sdcard.img
```


######### Uboot Script ##################

```
fatload mmc 1:2 ${loadaddr} partition-table.img
mmc dev 0
mmc write ${loadaddr} 0 0x42
fatload mmc 1:2 ${loadaddr} bootloader
mmc write ${loadaddr} 0x42 0xbb8

```