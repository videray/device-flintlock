LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := busybox
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := busybox
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)/sbin

# Create symlinks.
#LOCAL_POST_INSTALL_CMD := mkdir -p $(TARGET_RECOVERY_ROOT_OUT)/sbin; \
#    ln -sf busybox $(TARGET_RECOVERY_ROOT_OUT)/sbin/sh; \

include $(BUILD_PREBUILT)